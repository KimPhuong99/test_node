const { AuthService } = require('../services')
module.exports = {
    login: (req, res, next) => AuthService.login(req, res, next),
    signOut: (req, res, next) => AuthService.signOut(req, res, next),
    refreshToken: (req, res, next) => AuthService.getRefreshToken(req, res, next),
};