const { UserService } = require('../services')
module.exports = {
    signUp: (req, res, next) => UserService.createNewUser(req, res, next),
};