const bcrypt = require("bcrypt");
const saltRounds = 10;
const hashPassword = (pass) => bcrypt.hash(pass, saltRounds);
const comparePassword = (password, hash) => bcrypt.compare(password, hash);
module.exports = {
    hashPassword,
    comparePassword,
}