const signUp = require('./sign-up');
const auth = require('./auth');
module.exports = {
    signUp,
    auth,
}