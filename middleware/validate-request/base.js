const emailRegexValidator = (email) => email.match(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);

const isEmptyObject = (object, keys) => {
    for (let index = 0; index < keys.length; index++) {
        const element = keys[index];
        const value = object[element];
        if(!value || !value.length) return element;
    }
    return false;
}

module.exports = {
    emailRegexValidator,
    isEmptyObject,
}
