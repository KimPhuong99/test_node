const { emailRegexValidator, isEmptyObject } = require ("./base");

const loginValidator = (req, res, next) => {
    const body = req.body;
    const emptyElement = isEmptyObject(body, ['email', 'password']);
    if(emptyElement) return next({ status: 404, message: `${emptyElement} is empty!`});
    if(!emailRegexValidator(body.email)) return next({ status: 404, message: 'Email is invalid!' });
    if(body.password.length < 8 || body.password.length > 20) return next({ status: 404, message: 'Password is invalid!' })
    next();
}
module.exports = {
    loginValidator
}