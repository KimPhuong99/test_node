var express = require('express');
const { UserController } = require('../controllers');
const  { signUp }  = require('../middleware/validate-request');
var router = express.Router();
router.post('/sign-up', signUp.signUpValidator, UserController.signUp);

module.exports = router;