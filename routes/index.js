const userRouter = require('./user');
const authRouter = require('./auth')
module.exports = (app) => {
  app.use('/', userRouter);
  app.use('/', authRouter);
}