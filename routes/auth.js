var express = require("express");
var { expressjwt: jwt } = require("express-jwt");
const { AuthController } = require("../controllers");
const { auth } = require("../middleware/validate-request");
var router = express.Router();
router.post("/sign-in", auth.loginValidator, AuthController.login);
router.post(
  "/sign-out",
  jwt({
    secret: "access_secret",
    algorithms: ["HS256"],
  }),
  AuthController.signOut
);
router.post("/refresh-token", AuthController.refreshToken);

module.exports = router;
