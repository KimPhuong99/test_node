const dotenv = require('dotenv').config();
module.exports = {
    client: 'mysql2',
    connection: {
        host: process.env.HOST,
        port: process.env.PORT,
        user: process.env.USER_NAME,
        password: process.env.PASSWORD,
        database: process.env.DATABASE
    },
}