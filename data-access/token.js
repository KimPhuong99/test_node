const Base = require('./base');
const tableName = 'Tokens';
module.exports = (knex) => ({
    ...Base(knex, tableName),
    deleteByUserId: (userId) => knex(tableName).del().where({userId}),
});
