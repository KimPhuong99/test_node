const knex = require('../config/database');
const User = require('./user');
const Token = require('./token');
module.exports = {
    User: User(knex),
    Token: Token(knex),
}
  