const Base = require('./base');
const tableName = 'Users';
module.exports = (knex) => ({
    ...Base(knex, tableName),
});
