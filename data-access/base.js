module.exports = (knex, tableName) => ({
    creare: (props) => knex(tableName).insert(props.data),
    commonUpdate: (props) => knex(tableName).where(props.conditions).update(props.data),
    findById: (props) => knex(tableName).where({id: props.id}).select(...props.selects),
    deleteById: (props) => knex(tableName).del().where({id: props.id}),
    commonFind: (props) => knex(tableName).where(props.conditions).select(...props.selects),
});
