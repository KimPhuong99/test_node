const { hashPassword } = require("../helpers/bcrypt");
const { User } = require('../data-access');
const createNewUser = async (req, res, next) => {
    const {email, password, firstName, lastName} = req.body;
    const hashPass = await hashPassword(password);
    const newUser = {
       firstName,
       lastName,
       email,
       password: hashPass,
    };
    const newRecord = await User.creare({data: newUser});
    return res.status(201).json({ 
        id: newRecord.id,
        email,
        firstName,
        lastName,
        displayName: firstName + ' ' + lastName,
    });
}

module.exports = {
    createNewUser,
}
  