const jwt = require("jsonwebtoken");
const { comparePassword } = require("../helpers/bcrypt");
const { User, Token } = require("../data-access");
const login = async (req, res, next) => {
  const { email, password } = req.body;
  const user = await User.commonFind({
    conditions: {
      email,
    },
    selects: ["id", "email", "password", "firstName", "lastName"],
  }).then((results) => (results.length ? results[0] : null));
  if (!user) return next({ status: 401, message: `No such user found` });
  if (!comparePassword(password, user.password))
  return next({ status: 401, message: `Wrong password` });

  //TODO: set into .env
  const { accessToken, refreshToken } = await generateToken(
    user.id,
    user.email
  );
  const existedRefreshToken = await Token.commonFind({
    conditions: { userId: user.id },
    selects: [],
  });
  if (existedRefreshToken.length) {
    await Token.commonUpdate({
      conditions: { userId: user.id },
      data: { refreshToken, expiresIn: "30d" },
    });
  } else {
    await Token.creare({
      data: {
        userId: user.id,
        refreshToken,
        expiresIn: "30d",
      },
    });
  }
  return res.status(201).json({
    user: {
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      displayName: user.firstName + " " + user.lastName,
    },
    token: accessToken,
    refreshToken,
  });
};
const signOut = async (req, res, next) => {
  await Token.deleteByUserId(req.auth.userId);
  return res.status(204).json({});
};
const getRefreshToken = async (req, res, next) => {
  try {
    const token = req.body?.refreshToken;
  if (!token) return next({ status: 404, message: `Invalid refresToken` });
  const result = await jwt.verify(token, "refresh_secret", {
    algorithm: "HS256",
  });
  if (!result) return next( { status: 404, message: `Invalid refresToken` });
  const { userId, email } = result;
  const existedRefresToken = await Token.commonFind({
    conditions: { userId },
    selects: ["refreshToken"],
  }).then((result) => (result.length ? result[0] : null));
  if (!existedRefresToken) return next({ status: 500, message: `Internal error` });
  
  const { accessToken, refreshToken } = generateToken(
    userId,
    email
  );
  await Token.commonUpdate({
    conditions: { userId },
    data: { refreshToken, expiresIn: "30d" },
  });
  return res.status(200).json({
    token: accessToken,
    refreshToken,
  });
  } catch (err) {
    console.log(err);
    res.status(err.status || 500).json({ status: err.statusCode, message: err });
  }
};
const generateToken = (userId, email) => ({
  accessToken: jwt.sign(
    {
      userId: userId,
      email: email,
    },
    "access_secret",
    {
      algorithm: "HS256",
      expiresIn: 3000,
    }
  ),
  refreshToken: jwt.sign(
    {
      userId: userId,
      email: email,
    },
    "refresh_secret",
    {
      algorithm: "HS256",
      expiresIn: "30d",
    }
  ),
});
module.exports = {
  login,
  signOut,
  getRefreshToken,
};
