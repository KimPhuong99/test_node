var createError = require('http-errors');
var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const { json, urlencoded } = require('express');
const indexRouter = require('./routes');

var app = express();

app.use(logger('dev'));
app.use(json());
app.use(urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));

indexRouter(app)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  console.log('[ Error code ]', err.status );
  console.log('[ Error message ]', err.message );
  const statusCode = err.status || 500;
  res.status(statusCode).json({ status: statusCode, message: statusCode===500 ? 'Internal error' : err.message });
});

module.exports = app;
